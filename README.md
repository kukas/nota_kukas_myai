nota_kukas_myai 0.0.1
====

NOTA AI, homeworks for [HLAA](https://gamedev.cuni.cz/study/courses-history/courses-2020-2021/labs-for-human-like-artificial-agents-summer-202021/) at Matfyz.

Orders
----

* sandsail = first homework

* ctp2 = second homework

* [dependencies](./dependencies.json)

