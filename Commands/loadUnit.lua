function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load a given unit",
		parameterDefs = {
			{ 
				name = "unitToRescueID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitTransportID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

--[[
Node name: Load unit
Parameters
	unitID we would like to be rescued
	unitID of the transporter
Definition of SUCCESS: 
	“Unit to be rescued” is loaded in “transporter”
Definition of FAILURE: 
	“Unit to be rescued” is dead
	“Unit to be rescued” is loaded by other trasnporter
	“Unit to be rescued” is not transportable
	“Transporter” is dead
	“Transporter” is not transporter
	“They are far away from each other”
	Unit stops to execute “Spring” order
Definition of RUNNING: 
	Not success, Not FAILURE, 
Init (only first time): 
	Check all FAILUREure conditions
	Give “Spring” order to transporter to load a unit
Once running (always): 
	Check all FAILUREure conditions
	Check all success conditions
]]--

function Run(self, units, parameter)
	-- Once running (always): 
	-- FAILURE CONDITIONS
	-- “Unit to be rescued” is dead
	if parameter.unitToRescueID == nil or not Spring.ValidUnitID(parameter.unitToRescueID) then
		-- Logger.warn("invalid unitToRescueID")
		return FAILURE
	end
	-- “Transporter” is dead
	if parameter.unitTransportID == nil or not Spring.ValidUnitID(parameter.unitTransportID) then
		-- Logger.warn("invalid unitTransportID")
		return FAILURE
	end
	-- “Transporter” is not transporter
	-- “They are far away from each other”
	-- Unit stops to execute “Spring” order
	-- https://trello.com/c/qY9BfW2L/49-how-to-manipulate-with-command-queue
	
	-- SUCCESS CONDITIONS
	-- Check all success conditions
	if Spring.GetUnitTransporter(parameter.unitToRescueID) ~= nil then
		return SUCCESS
	end	
	
	-- first time
	if not self.initialized then
		Spring.GiveOrderToUnit(parameter.unitTransportID, CMD.LOAD_UNITS,{parameter.unitToRescueID},{"shift"})
		self.initialized = true
	end
	
	return RUNNING
end

function Reset(self)
	self.initialized = false
end
