function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unloads units at a position",
		parameterDefs = {
			{ 
				name = "unloadPosition",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
						{ 
				name = "unloadRadius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitToRescueID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitTransportID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

function Run(self, units, parameter)
	-- Once running (always): 
	-- FAILURE CONDITIONS
	-- “Unit to be rescued” is dead
	if parameter.unitToRescueID == nil or not Spring.ValidUnitID(parameter.unitToRescueID) then
		-- Logger.warn("invalid unitToRescueID")
		return FAILURE
	end
	-- “Transporter” is dead
	if parameter.unitTransportID == nil or not Spring.ValidUnitID(parameter.unitTransportID) then
		-- Logger.warn("invalid unitTransportID")
		return FAILURE
	end
	-- “Transporter” is not transporter
	-- “They are far away from each other”
	-- Unit stops to execute “Spring” order
	-- https://trello.com/c/qY9BfW2L/49-how-to-manipulate-with-command-queue
	
	-- SUCCESS CONDITIONS
	-- Check all success conditions
	if Spring.GetUnitTransporter(parameter.unitToRescueID) == nil then
		-- Spring.Echo("unit rescued!")
		return SUCCESS
	end	

	
	-- first time
	if not self.initialized then
		Spring.GiveOrderToUnit(parameter.unitTransportID, CMD.UNLOAD_UNITS, {parameter.unloadPosition.x, parameter.unloadPosition.y, parameter.unloadPosition.z, parameter.unloadRadius},{"shift"})
		self.initialized = true
	end
	
	return RUNNING
end

function Reset(self)
	self.initialized = false
end
