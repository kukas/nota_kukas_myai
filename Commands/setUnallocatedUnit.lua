function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Set the state of a unit to unallocated",
		parameterDefs = {
			{ 
				name = "unitManager",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "state",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unallocated",
			},
		}
	}
end

function Run(self, units, parameter)
	if parameter.unitId == nil then
		Logger.warn("setUnallocatedUnit: unitId is nil")
		return FAILURE
	end
	parameter.unitManager[parameter.unitId] = parameter.state
	return SUCCESS
end

function Reset(self)
	self.initialized = false
end
