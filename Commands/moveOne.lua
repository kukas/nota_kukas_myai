function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load a given unit",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<unitID>",
			},
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "Vec3(...)",
			},
		}
	}
end

-- constants
local THRESHOLD_STEP = 10
local THRESHOLD_DEFAULT = 0
local NOT_MOVING_THRESHOLD = 5

-- credits: based on formation.moveCustomGroup
function Run(self, units, parameter)
	local targetPosition = parameter.position -- Vec3
	local unitID = parameter.unitID -- int

	if not self.initialized then
		self.threshold = THRESHOLD_DEFAULT
		self.lastUnitPosition = Vec3(0,0,0)
		Spring.GiveOrderToUnit(unitID, CMD.MOVE, targetPosition:AsSpringVector(), {})
		self.initialized = true
	end


	local pointX, pointY, pointZ = Spring.GetUnitPosition(unitID)
	local unitPosition = Vec3(pointX, pointY, pointZ)
	
	-- threshold of success
	-- Spring.Echo(unitPosition:Distance(self.lastUnitPosition))
	if self.lastUnitPosition ~= nil and unitPosition:Distance(self.lastUnitPosition) < NOT_MOVING_THRESHOLD then 
		self.threshold = self.threshold + THRESHOLD_STEP 
	else
		self.threshold = THRESHOLD_DEFAULT
	end
	self.lastUnitPosition = unitPosition

	-- ignore the y coordinate when computing distance
	targetPosition.y = unitPosition.y
	if (unitPosition:Distance(targetPosition) < self.threshold) then
		return SUCCESS
	else
		
		return RUNNING
	end
end


function Reset(self)
	self.initialized = false
	self.threshold = THRESHOLD_DEFAULT
	self.lastUnitPosition = Vec3(0,0,0)
end
