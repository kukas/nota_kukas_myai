local sensorInfo = {
	name = "updateSavedUnitManager",
	desc = "Updates the state of all units in UnitManager according to whether they are in the safeArea or not",
	author = "Jiri Balhar",
	date = "2021-05-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return array with a structure {unitId = "unallocated"}
return function(unitManager, safeArea)
	for unitId, state in pairs(unitManager) do
		if state == "unallocated" or state == "safe" then
			local x, y, z = Spring.GetUnitPosition(unitId)
			local unitPosition = Vec3(x, y, z)

			if unitPosition:Distance(safeArea.center) <= safeArea.radius then
				unitManager[unitId] = "safe"
			else
				unitManager[unitId] = "unallocated"
			end
		end

	end
	return unitManager
end