local sensorInfo = {
	name = "GetPlateaus",
	desc = "Return the centroids of thresholded peaks on the map.",
	author = "Jiri Balhar",
	date = "2021-05-23",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = math.huge -- we do the computation only once

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

-- finds a continuous area using BFS and returns list of points that make up the area
-- warning: we mutate the peakGrid argument!
function fillArea(peakGrid, startIndices)
	local visited = {}
	local queue = {startIndices}
	while #queue > 0 do
		local current = table.remove(queue, 1) -- pop the front
		local i,j = unpack(current)
		if peakGrid[i][j] then
			table.insert(visited, current)
			peakGrid[i][j] = false
			table.insert(queue, {i+1, j})
			table.insert(queue, {i-1, j})
			table.insert(queue, {i, j+1})
			table.insert(queue, {i, j-1})
		end
	end
	return visited
end

-- computes the average x, y of a list of points
function getCentroid(areaGridPoints)
	sum = {0, 0}
	count = 0
	for _, point in ipairs(areaGridPoints) do
		i, j = unpack(point)
		sum[1] = sum[1] + i
		sum[2] = sum[2] + j
		count = count +1
	end
	return {sum[1]/count, sum[2]/count}
end

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
-- inspired by: nota_hlaa2019b_common/MapCanyons.lua
return function(minHeight, gridSize)
	gridSize = gridSize or 20
	local peakGrid = {}
	local peakIndices = {}

	for i = 1, Game.mapSizeX/gridSize do
		peakGrid[i] = {}
		for j = 1, Game.mapSizeZ/gridSize do
			local x, z = i*gridSize, j*gridSize
			y = Spring.GetGroundHeight(x, z)
			peakGrid[i][j] = y >= minHeight

			if peakGrid[i][j] then
				table.insert(peakIndices, {i,j})
			end
		end
	end

	-- go through each peak point and try to find a continuous region around it
	-- if it hasn't been found yet, compute the centroid of this region and return it
	result = {}
	for p = 1, #peakIndices do
		i, j = unpack(peakIndices[p])
		if peakGrid[i][j] then
			filled = fillArea(peakGrid, peakIndices[p])
			-- Spring.Echo("filled"..#filled)
			peakCentroid = getCentroid(filled)
			-- Spring.Echo(peakCentroid)
			x, z = unpack(peakCentroid)
			x = x*gridSize
			z = z*gridSize
			table.insert(result, {x = x, z = z})
			if (Script.LuaUI('exampleDebug_update')) then
				Script.LuaUI.exampleDebug_update(
					p, -- key
					{	-- data
						startPos = Vec3(x,190,z),
						endPos = Vec3(x,250,z)
					}
				)
			end
		end
	end
	
	return result
end