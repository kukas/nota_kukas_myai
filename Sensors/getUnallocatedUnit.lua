local sensorInfo = {
	name = "createUnitManager",
	desc = "Create unit manager. Allocate table with unit ids and their initial state",
	author = "Jiri Balhar",
	date = "2021-05-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return the first available unit
return function(unitManager, nearestToPosition)
	local selectedUnitId = nil
	local selectedDistance = math.huge
	for unitId, state in pairs(unitManager) do
		if state == "unallocated" and Spring.ValidUnitID(unitId) then
			if nearestToPosition == nil then
				selectedUnitId = unitId
				break
			else
				local x, y, z = Spring.GetUnitPosition(unitId)
				local unitPosition = Vec3(x, y, z)

				local dist = nearestToPosition:Distance(unitPosition)
				if dist < selectedDistance then
					selectedUnitId = unitId
					selectedDistance = dist
				end
			end
		end
	end
	if selectedUnitId == nil then
		return nil
	else
		unitManager[selectedUnitId] = "allocated"
		return selectedUnitId
	end
end