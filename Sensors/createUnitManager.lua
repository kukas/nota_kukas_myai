local sensorInfo = {
	name = "createUnitManager",
	desc = "Create unit manager. Allocate table with unit ids and their initial state",
	author = "Jiri Balhar",
	date = "2021-05-30",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return array with a structure {unitId = "unallocated"}
return function(unitIds)
	local unitManager = {}
	for i = 1, #unitIds do
		local unitId = unitIds[i]
		unitManager[unitId] = "unallocated"
	end
	return unitManager
end